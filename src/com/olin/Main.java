package com.olin;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

public class Main {
    public static final int PERIOD_SLEEP = 1000;
    public static volatile int counter = 0;
    public static final Object lockObject = new Object();
    public static volatile HashSet uniqueNumbers = new HashSet();

    public static void main(String[] args) {
	    Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(uniqueNumbers.size() < 100) {
                    try {
                        Thread.sleep(PERIOD_SLEEP);
                        Random rand = new Random();
                        int randomInt = rand.nextInt(100);
                        System.out.println(randomInt);
                        counter++;
                        synchronized (lockObject) {
                            uniqueNumbers.add(randomInt);
                            lockObject.notifyAll();
                        }
                    } catch (InterruptedException e) {
                        e.getStackTrace();
                    }
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lockObject) {
                    try {
                        while(uniqueNumbers.size() < 100) {
                            if(counter % 5 == 0)
                            {
                                System.out.println("Size: " + uniqueNumbers.size());
                                System.out.println("Found next unique numbers:" + Arrays.asList(uniqueNumbers));
                            }
                            lockObject.wait();
                        }

                        if(uniqueNumbers.size() >= 99)
                            System.out.println("Finish unique numbers array:" + Arrays.asList(uniqueNumbers));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        t1.start();
        t2.start();
    }
}
